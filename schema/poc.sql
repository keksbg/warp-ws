--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: list; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.list (
    id integer NOT NULL,
    accountid integer NOT NULL,
    hash character varying NOT NULL
);


ALTER TABLE public.list OWNER TO postgres;

--
-- Name: list_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.list_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.list_id_seq OWNER TO postgres;

--
-- Name: list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.list_id_seq OWNED BY public.list.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    uid integer NOT NULL,
    username character varying NOT NULL,
    password character varying NOT NULL,
    email character varying NOT NULL,
    cookie character varying,
    salt character varying,
    legacy boolean DEFAULT false,
    perms integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_uid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_uid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_uid_seq OWNER TO postgres;

--
-- Name: users_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_uid_seq OWNED BY public.users.uid;


--
-- Name: list id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.list ALTER COLUMN id SET DEFAULT nextval('public.list_id_seq'::regclass);


--
-- Name: users uid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN uid SET DEFAULT nextval('public.users_uid_seq'::regclass);


--
-- Data for Name: list; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.list (id, accountid, hash) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (uid, username, password, email, cookie, salt, legacy, perms) FROM stdin;
2	example	693bb8377f4b9b23c6bbce0b69ea472e40a3c32d5464240ee00e83041d6066a2e961b96decd31ded424323d444f1cb2fb47fd32838892ee8171bdfe822c2cd47	example@keksbg.dev	EzKTfuDOmShpr9Lp1mwGgtDvSk2TFnSehZFgu6rtfOU3LdpGqI0ONIekzPel59kZ	\N	t	8
4	sexy	d9d29e12ada2f64a951caf1f3e7dc1dfbc13fcc046ca1b482378a07440efa73f87f8d39c2a450e1e087b0777f511ed189da4151d7ad56c8514685aee62fcc51f	sexy@keksbg.dev	JzY0W50CwPDpskQYEeTJ5BRuSKFQOR1UxI63h6uwBpT7twKnck6TlxYwjjkcGjEi	P4L1Aa7bkAPEAEFd	f	0
16	sexyy	058c59b4acde2c7c5a7c9361b6c487a0e96db16fd4e11eacfd8bcf3533d9fa74d9405f64d0eae88b3cd1d0ab9949f4eb66d5b4f0102dac6c1c737222e123fc67	sexya@keksbg.dev	\N	82nSc2ItOF2gRdNW	f	0
1	keksbg	sex	me@keksbg.dev	\N	\N	t	0
3	sex	8ddf21d72b76600527df01c25d70fc64f3cd8c670331deb607c58b094e818770c0bb9387dacccce3ca0f3a9e6c0317ba387cf3e06219623e64da841572477496	sex@keksbg.dev	VsJrx4GLIZCkgAJynpdspAIiE3Q5eAti24OHyfROcn5mW5HnwYmZCsm7EBsgMVgN	\N	t	0
\.


--
-- Name: list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.list_id_seq', 1, false);


--
-- Name: users_uid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_uid_seq', 18, true);


--
-- Name: list list_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.list
    ADD CONSTRAINT list_pkey PRIMARY KEY (accountid);


--
-- Name: users salt_uniqueness; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT salt_uniqueness UNIQUE (salt);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (uid);


--
-- PostgreSQL database dump complete
--

