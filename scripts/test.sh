#!/bin/bash
# just for reference if it breaks
rustc --version && cargo --version
cargo test
cargo clippy -- -D warnings
echo 'checking formatting...'
if cargo fmt -- --check > /dev/null; then
    echo 'formatting is good!'
    exit 0
else
    echo 'bad formatting; fix by running scripts/format.sh'
    exit 1
fi