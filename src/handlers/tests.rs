use crate::filters;
use metrics_runtime::observers::PrometheusBuilder;
use metrics_runtime::Receiver;
use rand::distributions::Alphanumeric;
use rand::rngs::OsRng;
use rand::Rng;
use sqlx::PgPool;
use std::env;
use std::sync::Arc;
use warp::http::StatusCode;
use warp::test::request;

#[tokio::test]
async fn test_auth() {
	let db = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let receiver = Receiver::builder()
		.build()
		.expect("[METRICS] failed to create receiver");
	let sink = receiver.sink();
	let api = filters::todos(
		db,
		sink,
		receiver.controller(),
		Arc::new(PrometheusBuilder::new()),
	);
	let res = request()
		.method("POST")
		.path("/api/v1/authenticate")
		.header("content-type", "application/x-www-form-urlencoded")
		.header("user-agent", "AutoTest")
		.body("usermail=sex@keksbg.dev&password=sex")
		.reply(&api);
	let response = res.await;
	println!("{:?}", response);
	assert_eq!(response.status(), StatusCode::from_u16(200).unwrap());
}

#[tokio::test]
async fn test_deauth_wrong() {
	let db = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let receiver = Receiver::builder()
		.build()
		.expect("[METRICS] failed to create receiver");
	let sink = receiver.sink();
	let api = filters::todos(
		db,
		sink,
		receiver.controller(),
		Arc::new(PrometheusBuilder::new()),
	);
	let res = request()
		.method("GET")
		.path("/api/v1/logout")
		.header("Cookie", "token=asdf")
		.header("user-agent", "AutoTest")
		.body("")
		.reply(&api);
	let response = res.await;
	println!("{:?}", response);
	assert_eq!(response.status(), StatusCode::from_u16(401).unwrap());
}

#[tokio::test]
async fn test_deauth() {
	let cookie: String = OsRng.sample_iter(Alphanumeric).take(64).collect();
	println!("testing deauth (valid cookie, not in DB)");
	let db = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let receiver = Receiver::builder()
		.build()
		.expect("[METRICS] failed to create receiver");
	let sink = receiver.sink();
	let api = filters::todos(
		db,
		sink,
		receiver.controller(),
		Arc::new(PrometheusBuilder::new()),
	);
	let res = request()
		.method("GET")
		.path("/api/v1/logout")
		.header("Cookie", format!("token={}", cookie))
		.header("user-agent", "AutoTest")
		.body("")
		.reply(&api);
	let response = res.await;
	println!("{:?}", response);
	assert_eq!(response.status(), StatusCode::from_u16(401).unwrap());
}

#[tokio::test]
async fn create_user() {
	println!("testing user creation");
	let db = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let bruh = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let receiver = Receiver::builder()
		.build()
		.expect("[METRICS] failed to create receiver");
	let sink = receiver.sink();
	let api = filters::todos(
		db,
		sink,
		receiver.controller(),
		Arc::new(PrometheusBuilder::new()),
	);
	let res = request()
		.method("POST")
		.path("/api/v1/create-account")
		.header("content-type", "application/x-www-form-urlencoded")
		.header("user-agent", "AutoTest")
		.body("username=veryepic&password=epicstyle&email=sexyman@example.com")
		.reply(&api);
	let response = res.await;
	sqlx::query!("DELETE FROM public.users WHERE username = $1", "veryepic")
		.execute(&bruh)
		.await
		.unwrap();
	assert_eq!(response.status(), http::StatusCode::from_u16(201).unwrap());
}

#[tokio::test]
async fn full_test() {
	println!("testing full flow (create user, authenticate, deauthenticate and delete)");
	let db = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let receiver = Receiver::builder()
		.build()
		.expect("[METRICS] failed to create receiver");
	let sink = receiver.sink();
	let api = filters::todos(
		db,
		sink,
		receiver.controller(),
		Arc::new(PrometheusBuilder::new()),
	);
	let bruh = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();
	let ress = request()
		.method("POST")
		.path("/api/v1/create-account")
		.header("content-type", "application/x-www-form-urlencoded")
		.header("user-agent", "AutoTest")
		.body("username=veryepic&password=epicstyle&email=sexyman@example.com")
		.reply(&api);
	let responsee = ress.await;
	let res = request()
		.method("POST")
		.path("/api/v1/authenticate")
		.header("content-type", "application/x-www-form-urlencoded")
		.header("user-agent", "AutoTest")
		.body("usermail=veryepic&password=epicstyle")
		.reply(&api);
	let response = res.await;
	println!("Authentication: {:?}", response);
	let cookie = response
		.headers()
		.get("set-cookie")
		.unwrap()
		.to_str()
		.unwrap()
		.split(";")
		.collect::<Vec<&str>>()[0]
		.split("=")
		.collect::<Vec<&str>>()[1];
	let logoutres = request()
		.method("GET")
		.path("/api/v1/logout")
		.header("Cookie", format!("token={}", cookie))
		.header("user-agent", "AutoTest")
		.body("")
		.reply(&api);
	let logoutresponse = logoutres.await;
	println!("Deauthentication: {:?}", logoutresponse);
	assert_eq!(response.status(), http::StatusCode::from_u16(200).unwrap());
	assert_eq!(
		logoutresponse.status(),
		http::StatusCode::from_u16(200).unwrap()
	);
	assert_eq!(responsee.status(), http::StatusCode::from_u16(201).unwrap());
	sqlx::query!("DELETE FROM public.users WHERE username = $1", "veryepic")
		.execute(&bruh)
		.await
		.unwrap();
}
