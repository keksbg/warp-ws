#![feature(async_closure)]
#![deny(clippy::all)]
#![deny(warnings)]
#[macro_use]
extern crate log;

use metrics_runtime::observers::PrometheusBuilder;
use metrics_runtime::Receiver;
use sqlx::PgPool;
use std::env;
use std::sync::Arc;
use tokio::time::Duration;
use warp::Filter;

extern crate pretty_env_logger;

#[tokio::main]
async fn main() {
	if env::var_os("RUST_LOG").is_none() {
		// Set `RUST_LOG=todos=debug` to see debug logs,
		// this only shows access logs.
		env::set_var("RUST_LOG", "todos=info");
	}
	let listen = [0, 0, 0, 0];
	let port = 3030;
	let receiver = Receiver::builder()
		.histogram(Duration::from_secs(5), Duration::from_secs(1))
		.build()
		.expect("[METRICS] failed to create receiver");
	let sink = receiver.sink();
	let observer = Arc::new(PrometheusBuilder::new());
	pretty_env_logger::init();

	let db = PgPool::builder()
		.max_size(10)
		.build(&env::var("DATABASE_URL").unwrap())
		.await
		.unwrap();

	let api = filters::todos(db, sink, receiver.controller(), observer);

	let routes = api.with(warp::log("todos"));
	// Start up the server...
	warp::serve(routes).run((listen, port)).await;
	info!("Server ready. Listening @ 127.0.0.1:{}", port);
}

mod filters;

mod handlers;
