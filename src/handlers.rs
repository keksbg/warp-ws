use blake2::{Blake2b, Digest};
use chrono::{Duration, Utc};
use http::{header, Response};
use metrics_core::{Builder, Drain, Observe};
use metrics_runtime::observers::PrometheusBuilder;
use metrics_runtime::{Controller, Sink};
use rand::distributions::Alphanumeric;
use rand::rngs::OsRng;
use rand::Rng;
use redis::RedisResult;
use regex::Regex;
use sqlx::{PgConnection, Pool};
use std::collections::HashMap;
use std::convert::Infallible;
use std::env;
use std::ops::Add;
use std::sync::Arc;
use warp::Reply;

/// enum containing all permissions
///
/// `admin = 1 << 3`
///
/// [...]
enum Perms {
	ADMIN = 1 << 3, // 8
}

/// `POST /api/v1/authenticate`
///
/// Accepts only simple forms
///
/// **On success**: Redirects to `/` and sets a cookie with the
/// authentication ticket/token value
///
/// **On wrong password**: Returns back to login frontend and sends a
/// message.
pub async fn authenticate(
	data: HashMap<String, String>,
	db: Pool<PgConnection>,
	mut sink: Sink,
) -> Result<impl Reply, Infallible> {
	let client = redis::Client::open(env::var("REDIS_URL").unwrap()).unwrap();
	let mut con = client.get_connection().unwrap();
	sink.increment_counter("requests", 1);
	let time: u64;
	let start = sink.now();
	let mut correct: bool = false;
	let mut id: i32 = 0;
	trace!("generating cookie");
	let cookiestart = sink.now();
	let cookie: String = OsRng.sample_iter(Alphanumeric).take(64).collect();
	trace!("cookie gen: {}ns", sink.now() - cookiestart);
	trace!("fetching user from database");
	sqlx::query!(
		"SELECT uid,password,salt FROM public.users WHERE email = $1 OR username = $1",
		data.get("usermail")
	)
	.fetch_one(&db)
	.await
	.map_or_else(
		|_| {
			{
				debug!(
					"No account in database for \"{}\"",
					data.get("usermail").unwrap()
				);
			}
		},
		|r| {
			{
				id = r.uid;
				let password = data.get("password").unwrap();
				let res;
				if r.salt.is_some() {
					res = hash(String::from(password), Some(&r.salt.unwrap()));
				} else {
					res = hash(String::from(password), None);
				}
				trace!("Comparing database result for password and input hash for password");
				trace!("blake2b hashed: {}", res);
				trace!("blake2b in db : {}", r.password);
				if r.password == res {
					debug!("Correct password. Setting `correct` to `true`");
					correct = true;
				} else {
					debug!(
						"Incorrect password. Not changing anything since `correct` is `false` \
						 already."
					)
				}
			}
		},
	);
	trace!("Checking if `correct` == `true`");
	let resp;
	if correct {
		trace!("True");
		let _: () = redis::cmd("SET")
			.arg(format!("accounts:{}", id))
			.arg(cookie.clone())
			.arg("EX")
			.arg(8 * 60 * 60)
			.query(&mut con)
			.unwrap();
		resp = Response::builder()
			.header(
				header::SET_COOKIE,
				format!(
					"token={}; SameSite=Strict; HttpOnly; Expires={}",
					cookie,
					Utc::now().add(Duration::hours(8)).to_rfc2822()
				),
			)
			.header(
				header::SET_COOKIE,
				format!(
					"id={}; SameSite=Strict; HttpOnly; Expires={}",
					id,
					Utc::now().add(Duration::hours(8)).to_rfc2822()
				),
			)
			.header(header::LOCATION, "/")
			.status(200)
			.body("")
			.unwrap();
	} else {
		sink.increment_counter("auth_failed", 1);
		trace!("False");
		resp = Response::builder()
			.status(400)
			.body("Invalid username/email or password")
			.unwrap();
	}
	time = sink.now() - start;
	debug!("req time: {}", time);
	if !correct {
		sink.record_timing_with_labels("auth_time", start, sink.now(), &[("failed", "true")])
	} else {
		sink.record_timing_with_labels("auth_time", start, sink.now(), &[("failed", "false")]);
	}

	Ok(resp)
}

/// GET /api/v1/logout
///
/// Destroys the current session (sets cookie to `None` and removes it from
/// the DB)
///
/// Redirects to `/`
pub async fn sess_destroy(
	token: String,
	id: String,
	_db: Pool<PgConnection>,
	mut sink: Sink,
) -> Result<impl Reply, Infallible> {
	sink.increment_counter("requests", 1);
	let client = redis::Client::open(env::var("REDIS_URL").unwrap()).unwrap();
	let mut con = client.get_connection().unwrap();
	let start = sink.now();
	if check_cookie(token.clone(), id.clone()).await {
		let resp = Response::builder()
			.status(200)
			.header(
				header::SET_COOKIE,
				format!(
					"token={}; SameSite=Strict; HttpOnly; Expires={}",
					token,
					Utc::now().to_rfc2822()
				),
			)
			.header(header::LOCATION, "/")
			.header(
				header::SET_COOKIE,
				format!(
					"id={}; SameSite=Strict; HttpOnly; Expires={}",
					id,
					Utc::now().to_rfc2822()
				),
			)
			.body("")
			.unwrap();
		redis::cmd("DEL")
			.arg(format!("accounts:{}", id))
			.execute(&mut con);
		sink.record_timing("logout_time", start, sink.now());
		Ok(resp)
	} else {
		sink.increment_counter("invalid_session", 1);
		let resp = Response::builder()
			.status(401)
			.body("Invalid token; please refresh your browser cache.")
			.unwrap();
		sink.record_timing("logout_time", start, sink.now());
		Ok(resp)
	}
}

/// `POST /api/v1/create-account`, `content-type =
/// application/x-www-form-urlencoded`
///
/// Creates a new account with a random alphanumerical hash as defined in
/// `hash()`
///
/// Redirects to `/`
pub async fn create_account(
	data: HashMap<String, String>,
	db: Pool<PgConnection>,
	mut sink: Sink,
) -> Result<impl Reply, Infallible> {
	sink.increment_counter("requests", 1);
	let start = sink.now();
	let salt: String = OsRng.sample_iter(Alphanumeric).take(16).collect();
	let password = hash(String::from(data.get("password").unwrap()), Some(&salt));
	sqlx::query!(
		"INSERT INTO public.users(username, password, email, cookie, salt, perms) VALUES ($1, $2, \
		 $3, NULL, $4, 0)",
		data.get("username"),
		password,
		data.get("email"),
		&salt
	)
	.execute(&db)
	.await
	.unwrap();
	sink.record_timing("create_time", start, sink.now());
	Ok(Response::builder()
		.status(201)
		.header(header::LOCATION, "/")
		.body("")
		.unwrap())
}

pub async fn accounts(
	token: String,
	id: String,
	db: Pool<PgConnection>,
	mut sink: Sink,
) -> Result<impl warp::Reply, warp::Rejection> {
	sink.increment_counter("requests", 1);
	let start = sink.now();
	let mut invalid = false;
	let mut perms = false;
	if !check_cookie(token.clone(), id.clone()).await {
		invalid = true;
	} else {
		let r = sqlx::query!(
			"SELECT perms FROM public.users WHERE uid = $1",
			id.parse::<i32>().unwrap()
		)
		.fetch_one(&db)
		.await
		.unwrap();
		trace!("Verifying permissions.");
		if (r.perms.unwrap() & Perms::ADMIN as i32) == Perms::ADMIN as i32 {
			trace!("Setting perms to true.");
			perms = true;
		}
	}
	trace!("Checking if perms = true");
	if perms && !invalid {
		let res = sqlx::query_as!(models::User, "SELECT * FROM public.users")
			.fetch_all(&db)
			.await
			.unwrap();
		sink.record_timing("accounts_time", start, sink.now());
		Ok(warp::reply::json(&res))
	} else {
		sink.record_timing("accounts_time", start, sink.now());
		Err(warp::reject::custom(models::Error::AccessDenied))
	}
}

pub async fn self_rt(
	token: String,
	id: String,
	db: Pool<PgConnection>,
	mut sink: Sink,
) -> Result<impl warp::Reply, warp::Rejection> {
	sink.increment_counter("requests", 1);
	if !check_cookie(token.clone(), id.clone()).await {
		Err(warp::reject::custom(models::Error::Unauthorized))
	} else {
		sqlx::query_as!(
			models::StrippedUser,
			"SELECT uid, username, email, perms FROM users WHERE uid = $1",
			id.parse::<i32>().unwrap()
		)
		.fetch_one(&db)
		.await
		.map_or_else(
			|_| {
				sink.increment_counter("error_requests", 1);
				Err(warp::reject::custom(models::Error::DatabaseFailure))
			},
			|r| Ok(warp::reply::json(&r)),
		)
	}
}

pub async fn metrics(
	controller: Controller,
	builder: Arc<PrometheusBuilder>,
) -> Result<impl warp::Reply, warp::Rejection> {
	let mut observer = builder.build();
	controller.observe(&mut observer);
	Ok(observer.drain())
}

async fn check_cookie(cookie: String, id: String) -> bool {
	let client = redis::Client::open(env::var("REDIS_URL").unwrap()).unwrap();
	let mut con = client.get_connection().unwrap();
	let mut result = false;
	let cookie_regex = Regex::new(r"[a-zA-Z0-9]{64}").unwrap();
	if cookie_regex.is_match(cookie.as_str()) {
		let cmd: RedisResult<String> = redis::cmd("GET")
			.arg(format!("accounts:{}", id))
			.query(&mut con);
		if cmd.is_ok() && cmd.unwrap() == cookie {
			result = true;
		}
	}
	result
}

fn hash(password: String, salt: Option<&String>) -> String {
	let mut blake = Blake2b::new();
	blake.input(password.split_at(password.chars().count() / 2).0);
	if salt != None {
		blake.input(salt.unwrap());
	}
	blake.input(password.split_at(password.chars().count() / 2).1);
	format!("{:x}", blake.result())
}

/// Useful structs used for (de)serializing
pub mod models {
	use serde::{Deserialize, Serialize};
	use warp::reject::Reject;

	/// `struct User` is used for fitting queries into proper Rust structs.
	#[derive(Debug, Clone, Deserialize, Serialize)]
	pub struct User {
		pub uid: i32,
		pub username: String,
		pub password: String,
		pub email: String,
		pub cookie: Option<String>,
		pub salt: Option<String>,
		pub legacy: bool,
		pub perms: i32,
	}

	/// uid, username, email, perms
	#[derive(Debug, Clone, Deserialize, Serialize)]
	pub struct StrippedUser {
		pub uid: i32,
		pub username: String,
		pub email: String,
		pub perms: i32,
	}

	#[derive(Debug, Clone)]
	pub enum Error {
		DatabaseFailure,
		Unauthorized,
		AccessDenied,
	}

	#[derive(Debug, Clone, Deserialize, Serialize)]
	pub struct ErrorMessage {
		pub(crate) status: u16,
		pub(crate) message: String,
	}

	impl Reject for Error {}
}

#[cfg(test)]
mod tests;
