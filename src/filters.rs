use crate::handlers;
use http::StatusCode;
use metrics_runtime::observers::PrometheusBuilder;
use metrics_runtime::{Controller, Sink};
use sqlx::{PgConnection, Pool};
use std::sync::Arc;
use warp::{Filter, Rejection};

/// all the supposed routes together
pub fn todos(
	db: Pool<PgConnection>,
	sink: Sink,
	controller: Controller,
	observer: Arc<PrometheusBuilder>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	authenticate(db.clone(), sink.clone())
		.or(sess_destroy(db.clone(), sink.clone()))
		.or(create_account(db.clone(), sink.clone()))
		.or(self_rt(db.clone(), sink.clone()).recover(recover))
		.or(accounts(db, sink))
		.or(metrics(controller, observer))
}

/// POST /api/v1/authenticate
pub fn authenticate(
	db: Pool<PgConnection>,
	sink: Sink,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	warp::path!("api" / "v1" / "authenticate")
		.and(warp::post())
		.and(warp::body::content_length_limit(1024 * 32))
		.and(warp::body::form())
		.and(with_db(db))
		.and(with_sink(sink))
		.and_then(handlers::authenticate)
}

pub fn sess_destroy(
	db: Pool<PgConnection>,
	sink: Sink,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	warp::path!("api" / "v1" / "logout")
		.and(warp::get())
		.and(warp::filters::cookie::cookie("token"))
		.and(warp::filters::cookie::cookie("id"))
		.and(with_db(db))
		.and(with_sink(sink))
		.and_then(handlers::sess_destroy)
}

pub fn create_account(
	db: Pool<PgConnection>,
	sink: Sink,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	warp::path!("api" / "v1" / "create-account")
		.and(warp::post())
		.and(warp::body::content_length_limit(1024 * 32))
		.and(warp::body::form())
		.and(with_db(db))
		.and(with_sink(sink))
		.and_then(handlers::create_account)
}

pub fn accounts(
	db: Pool<PgConnection>,
	sink: Sink,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	warp::path!("api" / "v1" / "accounts")
		.and(warp::get())
		.and(warp::filters::cookie::cookie("token"))
		.and(warp::filters::cookie::cookie("id"))
		.and(with_db(db))
		.and(with_sink(sink))
		.and_then(handlers::accounts)
	// warp::get()
	// 	.and(end())
	// 	.and(warp::filters::cookie::cookie("token"))
	// 	.and(with_db(db))
	// 	.and(with_sink(sink))
	// 	.and_then(handlers::accounts)
}

pub fn self_rt(
	db: Pool<PgConnection>,
	sink: Sink,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	warp::path!("api" / "v1" / "self")
		.and(warp::get())
		.and(warp::filters::cookie::cookie("token"))
		.and(warp::filters::cookie::cookie("id"))
		.and(with_db(db))
		.and(with_sink(sink))
		.and_then(handlers::self_rt)
}

pub fn metrics(
	controller: Controller,
	observer: Arc<PrometheusBuilder>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
	warp::path!("metrics")
		.and(warp::get())
		.and(warp::any().map(move || controller.clone()))
		.and(warp::any().map(move || observer.clone()))
		.and_then(handlers::metrics)
}

fn with_db(
	db: Pool<PgConnection>,
) -> impl Filter<Extract = (Pool<PgConnection>,), Error = std::convert::Infallible> + Clone {
	warp::any().map(move || db.clone())
}

fn with_sink(
	sink: Sink,
) -> impl Filter<Extract = (Sink,), Error = std::convert::Infallible> + Clone {
	warp::any().map(move || sink.clone())
}

async fn recover(rej: Rejection) -> Result<impl warp::Reply, Rejection> {
	if let Some(rej) = rej.find::<handlers::models::Error>() {
		let err = match rej {
			handlers::models::Error::DatabaseFailure => {
				error!("Database seems down, check it.");
				handlers::models::ErrorMessage {
					status: 500,
					message: String::from("Database failure; please contact the administrator."),
				}
			},
			handlers::models::Error::Unauthorized => {
				handlers::models::ErrorMessage {
					status: 401,
					message: String::from(
						"You are not authorized, and thus do not have access to this page.",
					),
				}
			},
			handlers::models::Error::AccessDenied => {
				handlers::models::ErrorMessage {
					status: 403,
					message: String::from(
						"You do not have the appropriate permissions to access this page.",
					),
				}
			},
		};
		Ok(warp::reply::with_status(
			err.message,
			StatusCode::from_u16(err.status).unwrap(),
		))
	} else {
		Err(rej)
	}
}
