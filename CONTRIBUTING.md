Hello and thank you for deciding to contribute this project.

This was a small hobby of mine to learn proper Rust practices and different tools that get
utilized.

All contributions are welcome, but before you decide to make your own fork and create a merge
request, you should verify that your code will pass our automated tests in `scripts/test.sh`.

What is done is that the code is verified that it compiles and afterwards it is checked by
`clippy` to correct it for the best practices with Rust code, then checked by `rustfmt` to 
verify the formatting matches our style guidelines.

Please remember that all the code you contribute will be licensed under the Mozilla Public License 
version 2.0.
 