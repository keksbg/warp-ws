[![pipeline status](https://gitlab.com/keksbg/warp-ws/badges/master/pipeline.svg)](https://gitlab.com/keksbg/warp-ws/-/commits/master)
# warp-ws, a conceptual webserver written in Rust with the warp crate
## Why?
### Rust
Rust is a fast, efficient, thread-safe and memory-safe language that does not have garbage collection.

Whilst being syntactically similar to C++, it manages to add more to the standard C++ style and 
creates a more developer-friendly language whilst being similarly fast.

The Rust compiler is an amazing tool that you sometimes have to fight but then ultimately surrender
to because StackOverflow was able to fix the issue for you, whilst the compiler wouldn't give it up.

Thanks Rust compiler for not telling me the most obvious fix!

Putting aside the irony, the Rust compiler is a great tool that has some amazing potential that
can be used (for example, the [`sqlx`]() crate has a the `query!` macro that automatically verifies
that the required columns, tables and databases exist at **compile time**).
It is absolutely amazing how many things can be done with the help of this compiler.

### warp
`warp` is a crate that provides an async and really fast high-level crate based on the popular
lower-level `hyper` crate. `warp` supports WebSockets, standard HTTP requests and more, whilst remaining
simple to understand.

It can do ~30k req/s on an i7-6700k which is comprable to `node.js` using the `uWebsockets` library
(tested with the [`uWebsockets` testing suite](https://github.com/uNetworking/uWebSockets) )
## How?
If you'd like to run the program, just type `cargo run` in the root directory and you should be off!
Usually it listens at all IPs (`0.0.0.0`) at port `3030`.
